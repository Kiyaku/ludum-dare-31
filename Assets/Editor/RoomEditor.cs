﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(RoomData))]
public class RoomEditor : Editor {
	public Texture2D TileMap;
	private RoomData currentData;
	private List<Texture2D> textureTiles = new List<Texture2D>();
	private int selectedTexture = 0;
	private int tempTeturesPerRow = -1;
	private int tempRoomWidth = 0;
	private int tempRoomHeight = 0;


	[MenuItem("Custom/Create Room")]
	public static void CreateRoom() {
		string path = EditorUtility.SaveFilePanelInProject("Save Room", "New Room", "asset", "Save new room");

		RoomData roomData = CreateInstance<RoomData>();
		roomData.Init();

		AssetDatabase.CreateAsset(roomData, path);
		AssetDatabase.SaveAssets();
	}


	public override void OnInspectorGUI() {
		currentData = target as RoomData;

		if (currentData != null) {
			if (tempRoomWidth == 0)
				tempRoomWidth = currentData.RoomWidth;

			if (tempRoomHeight == 0)
				tempRoomHeight = currentData.RoomHeight;

			//tempRoomWidth = EditorGUILayout.IntField("Width: ", tempRoomWidth);
			//tempRoomHeight = EditorGUILayout.IntField("Height: ", tempRoomHeight);

			//if (GUILayout.Button("Update Room Size")) {
			//	if (EditorUtility.DisplayDialog("Resize room", "This will reset the room. Resize anyway?", "Resize", "Cancel")) {
			//		currentData.RoomWidth = tempRoomWidth;
			//		currentData.RoomHeight = tempRoomHeight;

			//		currentData.Init();
			//		return;
			//	}
			//}
			
			currentData.TileTexture = (Texture2D)EditorGUILayout.ObjectField("Tile Texture: ", currentData.TileTexture, typeof(Texture2D));

			if (currentData.TileTexture != null) {
				currentData.TexturesPerRow = EditorGUILayout.IntField("Textures per row: ", currentData.TexturesPerRow);

				if (currentData.TexturesPerRow != tempTeturesPerRow) {
					tempTeturesPerRow = currentData.TexturesPerRow;

					GetTexturesForAtlas();
				}

				DrawPalette();
				DrawGrid();
			}

			EditorUtility.SetDirty(currentData);
		}
	}


	private void GetTexturesForAtlas() {
		if (currentData.TexturesPerRow != 0) {
			textureTiles = Utils.GetTexturesForAtlas(currentData.TileTexture, currentData.TexturesPerRow);
		}
	}


	private void DrawPalette() {
		if (textureTiles.Count == 0 && currentData.TexturesPerRow != 0) {
			textureTiles = Utils.GetTexturesForAtlas(currentData.TileTexture, currentData.TexturesPerRow);
		}

		selectedTexture = GUILayout.SelectionGrid(selectedTexture, textureTiles.ToArray(), 8);
	}


	private void DrawGrid() {
		//GUILayout.BeginVertical();
		for (int y = currentData.RoomHeight - 1; y >= 0; y--) {
			//GUILayout.BeginHorizontal();

			for (int x = 0; x < currentData.RoomWidth; x++) {
				GUIContent button = new GUIContent(textureTiles[currentData.Tiles[x + y * currentData.RoomWidth].TextureID]);
				Rect rect = new Rect(x * 32, currentData.RoomHeight * 32 - (y * 32) + 150, 32, 32);
				
				if (Event.current.isMouse && rect.Contains(Event.current.mousePosition)) {
					currentData.Tiles[x + y * currentData.RoomWidth].TextureID = selectedTexture;
				}

				GUI.Button(rect, button, GUIStyle.none);
				//if (GUILayout.Button(button, GUIStyle.none, GUILayout.Width(32), GUILayout.Height(32))) {
				//	currentData.Tiles[x + y * currentData.RoomWidth].TextureID = selectedTexture;
				//}
			}
			
			//GUILayout.EndHorizontal();
		}

		//GUILayout.EndVertical();
	}
}
