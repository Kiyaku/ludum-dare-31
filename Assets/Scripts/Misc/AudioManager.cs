﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class AudioManager {
	public static bool PlayMusic = true;
	public static bool PlaySFX = true;

	public delegate void AudioDelegate();
	public static event AudioDelegate OnMusicChange;


	public static void Init() {
		PlayMusic = PlayerPrefs.GetInt("music", 1) == 1;
		PlaySFX = PlayerPrefs.GetInt("sfx", 1) == 1;
	}


	public static void ToggleMusic() {
		PlayMusic = !PlayMusic;

		if (OnMusicChange != null)
			OnMusicChange();

		PlayerPrefs.SetInt("music", PlayMusic ? 1 : 0);
	}


	public static void ToggleSFX() {
		PlaySFX = !PlaySFX;

		PlayerPrefs.SetInt("sfx", PlaySFX ? 1 : 0);
	}
}
