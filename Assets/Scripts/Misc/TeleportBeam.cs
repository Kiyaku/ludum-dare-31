﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TeleportBeam : MonoBehaviour {
	public void FadeIn(float speed) {
		StartCoroutine(Fade(0, 1, speed));
	}


	public void FadeOut(float speed) {
		StartCoroutine(Fade(1, 0, speed));
	}


	private IEnumerator Fade(float from, float to, float speed) {
		float timer = 0;
		float alpha = 0;

		while (timer < 1) {
			timer += Time.deltaTime * speed;
			alpha = Mathf.Lerp(from, to, timer);

			foreach (Transform beam in transform) {
				Color col = beam.renderer.material.GetColor("_TintColor");
				col.a = alpha;
				beam.renderer.material.SetColor("_TintColor", col);
			}

			yield return null;
		}
	}
}
