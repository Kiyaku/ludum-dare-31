﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimateLetters : MonoBehaviour, IInteractive {
	public List<Transform> LetterList = new List<Transform>();
	public Color NormalColor;
	public Color HighlightColor;
	public string MethodToExecute;


	private IEnumerator Start() {
		float angle = 0;

		SetColor(NormalColor);

		while (true) {
			angle -= Time.deltaTime * 300f;

			for (int i = 0; i < LetterList.Count; i++) {
				Vector3 pos = LetterList[i].localPosition;
				pos.y = Mathf.Lerp(0, 1f, Mathf.Cos((angle + 20f * i) * Mathf.Deg2Rad));
				LetterList[i].localPosition = pos;
			}

			if (angle >= 360)
				angle -= 360;

			yield return null;
		}
	}


	private void SetColor(Color col) {
		foreach (Transform letter in LetterList) {
			letter.renderer.material.color = col;
		}
	}


	public void Interact(Transform target) {
		GameManager.ins.SendMessage(MethodToExecute, SendMessageOptions.DontRequireReceiver);
		collider.enabled = false;
		gameObject.SetActive(false);
	}


	private void OnTriggerEnter(Collider col) {
		if (col.tag == "Player") {
			SetColor(HighlightColor);
		}
	}


	private void OnTriggerExit(Collider col) {
		if (col.tag == "Player") {
			SetColor(NormalColor);
		}
	}
}
