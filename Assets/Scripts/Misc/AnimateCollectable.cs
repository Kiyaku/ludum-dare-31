﻿using UnityEngine;
using System.Collections;

public class AnimateCollectable : MonoBehaviour
{
    private IEnumerator Start()
    {
        float angle = 0;

        while (true)
        {
            angle -= Time.deltaTime * 300.0f;

            Vector3 pos = transform.localPosition;
            pos.y = Mathf.Lerp(0, 1f, Mathf.Abs(Mathf.Cos((angle) * Mathf.Deg2Rad)));
            transform.localPosition = pos;

            if (angle >= 360)
                angle -= 360;

            yield return null;
        }
    }
}
