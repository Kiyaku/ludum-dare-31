﻿using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour {


    public int type;
	public AudioClip Audio;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //Collided with this collectable...
    void OnTriggerEnter(Collider player)
    {
		if (AudioManager.PlaySFX)
			Camera.main.audio.PlayOneShot(Audio, 2);

        player.gameObject.GetComponent<Player>().increaseStat(type);
        Destroy(this.gameObject);
    }
}
