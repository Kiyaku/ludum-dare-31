﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	public static GameManager ins;

	public delegate void GameDelegate();
	public static event GameDelegate OnStartGame;
	public static event GameDelegate OnEndGame;

	public bool IsGameRunning = false;
	public bool IsGameOver = false;
	public UILabel TutorialLabel;
	public UIInput NameInput;


	private void Awake() {
		AudioManager.Init();

		ins = this;
	}


	private void Update() {
		if (IsGameOver) {
			if (!NameInput.isSelected && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.JoystickButton0))) {
				Application.LoadLevel(Application.loadedLevel);
			}
		}

		if (Input.GetKeyDown(KeyCode.M)) {
			AudioManager.ToggleMusic();
		}

		if (Input.GetKeyDown(KeyCode.N)) {
			AudioManager.ToggleSFX();
		}
	}


	public void StartGame() {
		if (OnStartGame != null) {
			TutorialLabel.gameObject.SetActive(false);
			IsGameRunning = true;
			OnStartGame();
		}
	}


	public void EndGame() {
		if (OnEndGame != null) {
			IsGameRunning = false;
			IsGameOver = true;
			OnEndGame();
		}
	}
}
