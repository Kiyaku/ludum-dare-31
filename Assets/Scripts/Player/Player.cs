﻿using UnityEngine;
using System.Collections;

public class Player : CharacterBase 
{
    private PlayerMove _pmScript;

	// Use this for initialization
	void Start () 
    {
        health = 10;
        damage = 1;
        defense = 1;
        speed = 0;
		teleportationScrolls = 1;

		isPlayer = true;

		UIManager.ins.UpdateStat(0, health, false, false);
		UIManager.ins.UpdateStat(1, damage, false, false);
		UIManager.ins.UpdateStat(2, defense, false, false);
		UIManager.ins.UpdateStat(4, teleportationScrolls, true, false);

        name = "Player";
        _pmScript = this.gameObject.GetComponent<PlayerMove>();
	}

    public void increaseStat(int stat)
    {
		if (stat == 0) {
			health += 5;
			UIManager.ins.UpdateStat(stat, health, false);
		} else if (stat == 1) {
			damage++;
			UIManager.ins.UpdateStat(stat, damage, false);
		} else if (stat == 2) {
			defense++;
			UIManager.ins.UpdateStat(stat, defense, false);
		} else if (stat == 3) {
			speed++;
			_pmScript.speedTimer += 5;
			UIManager.ins.UpdateStat(stat, speed, false);
		} else if (stat == 4) {
			teleportationScrolls++;
			UIManager.ins.UpdateStat(stat, teleportationScrolls, true);
		}
    }

    public int getStat(int stat)
    {
        if (stat == 0)
            return health;
        if (stat == 1)
            return damage;
        if (stat == 2)
            return defense;
        if (stat == 3)
            return speed;
        return -1;
    }


    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Collider col = hit.collider;
        //Check player movement here so we don't fire this twice...
        if (col.gameObject.name != "Frosty" || !_pmScript.canMove)
            return;
        
        _pmScript.canMove = false;
        StartCoroutine(Combat(col.gameObject));
        StartCoroutine(col.gameObject.GetComponent<CharacterBase>().Combat(gameObject));
    }

}
