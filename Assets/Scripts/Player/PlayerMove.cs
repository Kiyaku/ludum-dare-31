﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

class PlayerMove : MonoBehaviour {
	[System.Serializable]
	public class Speech {
		public string Text;
		public AudioClip[] Clips;
	}

    public float moveSpeed;
    public bool canMove = true;
	public Transform TeleportBeam;
    private Player _player;

    private Vector2 input;
	private float gravity = 0;
	private float speedModifier = 2;
	public int Kills = 0;
	public float speedTimer = 0;
	public ParticleSystem RunningParticles;

	public AudioClip TeleportStartAudio;
	public AudioClip TeleportEndAudio;
	public AudioClip FallingSound;

	private CharacterController controller;
	private IInteractive tempInteractive;

	private string[] speeches = new string[] { "Over here!", "Hey hey!", "I'm here!" };
	public List<Speech> Speeches = new List<Speech>();


	private void Awake() {
		controller = GetComponent<CharacterController>();

		TeleportBeam.parent = null;
		TeleportBeam.gameObject.SetActive(false);
        //The Player script and the Move script are in the same parent...
        _player = GetComponent<Player>();
	}


    public void Update()
    {
        //If we can't move, or we dead, bail here...
        if (!canMove || !_player.isAlive())
            return;

        //Get some input values...
		if (canMove && !GameManager.ins.IsGameOver) {
			// Check other input
			input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
			CheckControls();
		} else {
			input = Vector2.zero;
		}

        _player.playWalk(!(input == Vector2.zero));

		//How far we are moving this frame...
		Vector3 moveDir = new Vector3(input.x, 0, input.y);

		if (controller.isGrounded) {
			gravity = 0;
		} else {
			gravity -= 2f;
		}

		if (speedTimer > 0) {
			if(moveDir.magnitude > 0.1f) 
				RunningParticles.Emit(1);

			speedTimer -= Time.deltaTime;
			moveDir *= speedModifier;
		}

		moveDir.y = gravity;
		controller.Move(moveDir * moveSpeed * Time.deltaTime);

		// Check if player died
		if (!GameManager.ins.IsGameOver && transform.position.y <= -2) {
			GameManager.ins.EndGame();

			if (AudioManager.PlaySFX)
				audio.PlayOneShot(FallingSound);
			//canMove = false;
		}

		// Player rotation
		moveDir.y = 0;
		
		if (canMove && moveDir.magnitude > 0.01f) {
			Quaternion lookRot = Quaternion.LookRotation(-moveDir.normalized);
			transform.rotation = lookRot;
		}
    }


	private void CheckControls() {
		// Shouting
		if (Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.JoystickButton2)) {
			int speech = Random.Range(0, Speeches.Count);

			if (AudioManager.PlaySFX)
				audio.PlayOneShot(Speeches[speech].Clips[Random.Range(0, Speeches[speech].Clips.Length)]);

			UIManager.ins.ShowSpeechBubble(transform, Speeches[speech].Text);
		}


		// Interacting
		if (Input.GetKeyDown(KeyCode.E)) {
			if (tempInteractive != null) {
				tempInteractive.Interact(transform);
			}
		}


		// Teleporting
		if (Input.GetKeyDown(KeyCode.T) || Input.GetKeyDown(KeyCode.JoystickButton3)) {
			Teleport();
		}
	}


	public void Teleport() {
		if (!canMove)
			return;

		if (_player.teleportationScrolls <= 0) {
			return;
		}


		Room room = RoomGenerator.ins.GetRandomRoom();

		if (room != null) {
			_player.teleportationScrolls--;
			UIManager.ins.UpdateStat(4, _player.teleportationScrolls, true, false);

			Vector3 tilePos = room.GetSpawnableTiles();
			tilePos += new Vector3(0, 0.2f, 0f);

			StartCoroutine(AnimateTeleportation(tilePos));
		}
	}

	private IEnumerator AnimateTeleportation(Vector3 destination) {
		float timer = 0;
		float speed = 2;

		Vector3 newScale = new Vector3(0.1f, 10, 0.1f);
		Vector3 beamStartScale = new Vector3(2, 10000, 2);
		Vector3 beamEndScale = new Vector3(0, 10000, 0);

		canMove = false;

		if (AudioManager.PlaySFX)
			audio.PlayOneShot(TeleportStartAudio, 0.5f);

		TeleportBeam.position = transform.position;
		TeleportBeam.localScale = beamStartScale;
		TeleportBeam.gameObject.SetActive(true);
		TeleportBeam.GetComponent<TeleportBeam>().FadeIn(speed);

		while (timer < 1) {
			timer += Time.deltaTime * speed;

			transform.localScale = Vector3.Lerp(Vector3.one, newScale, timer);
			TeleportBeam.localScale = Vector3.Lerp(beamStartScale, beamEndScale, timer);

			yield return null;
		}

		transform.position = destination;
		TeleportBeam.position = transform.position;
		TeleportBeam.GetComponent<TeleportBeam>().FadeOut(speed);
		timer = 0;

		if (AudioManager.PlaySFX)
			audio.PlayOneShot(TeleportEndAudio, 0.5f);

		while (timer < 1) {
			timer += Time.deltaTime * speed;

			transform.localScale = Vector3.Lerp(newScale, Vector3.one, timer);
			TeleportBeam.localScale = Vector3.Lerp(beamEndScale, beamStartScale, timer);

			yield return null;
		}

		TeleportBeam.gameObject.SetActive(false);
		transform.localScale = Vector3.one;
		canMove = true;
	}


	private void OnTriggerEnter(Collider col) {
		IInteractive interact = col.GetComponent(typeof(IInteractive)) as IInteractive;

		if (interact != null && !GameManager.ins.IsGameRunning) {
			Destroy(col.gameObject);
			GameManager.ins.StartGame();
		}
	}


	private void OnTriggerExit(Collider col) {
		tempInteractive = null;
	}
}