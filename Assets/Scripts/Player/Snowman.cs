﻿using UnityEngine;
using System.Collections;

public class Snowman : CharacterBase 
{
    private static int spawns = 0;
    public static void reset() { spawns = 0; }

	void Start () 
    {
        health = 5;
        defense = 0;
        damage = 3;
        speed = 0;
        attackCooldown = 1.0f;
        name = "Frosty";
        Spawn();
	}
	
    //When we spawn a new snowman, we need to adjust his HP value to make them harder over time...
    public void Spawn()
    {
        spawns++;
        int buff = 1 + (spawns / 3); // Every 3 spawns, the buff is bigger...

		for (int i = 0; i < buff; i++) {
			// 0 = damage
			// 1 = defense
			// 2,3,4 = health
			int type = Random.Range(0, 5);

			if (type == 0) {
				damage++;
			} else if (type == 1) {
				defense++;
			} else {
				health++;
			}
		}
    }
}
