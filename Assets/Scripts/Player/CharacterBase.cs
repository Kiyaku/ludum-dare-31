﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterBase : MonoBehaviour 
{
    // Our stats...
    protected int health;
    protected int damage;
    protected int defense;
    protected int speed;

	public int teleportationScrolls;

	public List<Transform> Drops = new List<Transform>();
	public List<AudioClip> HitSounds = new List<AudioClip>();

    // Who you wanna kill...?
    protected GameObject enemy = null;
    // And how slowly can I do it...?
    protected float attackCooldown = 1.0f;
	protected bool isPlayer = false;

	private Transform tempParent;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    // I'm alive...?
    public bool isAlive()
    {
        return health > 0;
    }

    // Land a hit, deal some damage... 
    public void hit(int damage)
    {
        // Bit unfair to have unkillable enemies in unavoidable combat, so min damage is 1...
        // Also ensures that players will always take damage...
		damage = Mathf.Max(damage - defense, 1);
		health -= damage;

		health = Mathf.Clamp(health, 0, health);

		UIManager.ins.ShowDamageNumber(damage, transform.position);
        if ( isPlayer )
        {
            UIManager.ins.UpdateStat(0, health, true, false);
        }
        //Did we just die...?
        if (health <= 0)
        {
            //Yep... We dead...
            playDead();
        }
    }

    public void playWalk(bool start = true)
    {
        Animator anim = GetComponent<Animator>();
        if (anim != null)
            anim.SetBool("isMoving", start);
    }


    public void playDead()
    {
        //Turn off the collider...
        GetComponent<Collider>().enabled = false;

		if (Drops.Count > 0 && tempParent != null) {
			Transform spawnedObject = Instantiate(Drops[Random.Range(0, Drops.Count)]) as Transform;
			spawnedObject.position = transform.parent.position;
			spawnedObject.parent = tempParent;
		}

        Animator anim = GetComponent<Animator>();
        if ( anim != null)
            anim.SetBool("isDead", true);
    }

    public void playAttack()
    {
        Animator anim = GetComponent<Animator>();
        if (anim != null)
            anim.SetBool("isAttacking", true);
    }

    public void playIdle()
    {
        Animator anim = GetComponent<Animator>();
        if (anim != null)
            anim.SetBool("isAttacking", false);
    }

    public void setCombatAnim(bool value)
    {
        Animator anim = GetComponent<Animator>();
        if (anim != null)
        {
            anim.SetBool("isCombat", value);
        }
    }

    // Ooooh I love a good fight...!
    public IEnumerator Combat(GameObject enemy)
    {
        this.enemy = enemy;
        CharacterBase enemyStats = enemy.GetComponent<CharacterBase>();

		if(isPlayer)
			CameraControl.ins.SwitchCameraMode(CameraControl.CameraModes.FIGHTING);
		//KillCam.ins.ToggleCamera(true);

        //Rotate us to look at our target...
		Vector3 lookAtDir = transform.position - enemy.transform.position;
		lookAtDir.y = 0;

		if (!isPlayer) {
			tempParent = transform.parent.parent;
			transform.parent.parent = null;
			lookAtDir = -lookAtDir;
		}

		Quaternion lookRot = Quaternion.LookRotation(lookAtDir.normalized);
		transform.rotation = lookRot;

        setCombatAnim(true);

        while (true)
        {
            yield return new WaitForSeconds(attackCooldown);
            if (isAlive() && enemyStats.isAlive())
            {
				if (HitSounds.Count > 0) {
					if (AudioManager.PlaySFX)
						audio.PlayOneShot(HitSounds[Random.Range(0, HitSounds.Count)]);
				}

                enemyStats.hit(damage);
                playAttack();
            }
            else
                break;
        }

		if (isPlayer)
			CameraControl.ins.SwitchCameraMode(CameraControl.CameraModes.EXPLORING);
		//KillCam.ins.ToggleCamera(false);

        //Got out here, someone died... Cancel the coroutine...
        setCombatAnim(false);

        // I don't like this, it's really hacky...
        PlayerMove pmScript = this.gameObject.GetComponent<PlayerMove>();
        if (pmScript != null)
        {
            //We dead...?
            if (!isAlive())
            {
                // Ya mun...
                GameManager.ins.EndGame();
            }
            else
            {
				pmScript.Kills++;
                pmScript.canMove = true;
            }
        }
        yield return null;
    }

    // Here lies dear <insert name here>, beloved player/enemy...
    public IEnumerator Bury()
    {
        //Purpose of this function is to drop the character out of the level and despawn them...
        yield return new WaitForSeconds(1.0F);
        float t = 0;
        while (t < 1.0f)
        {
            t += Time.deltaTime;
            transform.position -= new Vector3(0.0f, Time.deltaTime, 0.0f);
            yield return null;
        }

        Destroy(transform.parent.gameObject);
        yield return null;
    }
}
