﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class Utils {
	public static List<Texture2D> GetTexturesForAtlas(Texture2D atlas, int rowCount) {
		List<Texture2D> textureList = new List<Texture2D>();
		int textureSize = atlas.width / rowCount;

		if (atlas != null) {
			for(int y = rowCount - 1; y >= 0; y--) {
				for (int x = 0; x < rowCount; x++) {
					Texture2D newTex = new Texture2D(textureSize, textureSize, TextureFormat.RGBA32, false);

					newTex.SetPixels(atlas.GetPixels(x * textureSize, y * textureSize, textureSize, textureSize));
					newTex.Apply();
					textureList.Add(newTex);
				}
			}
		}

		return textureList;
	}


	public static void Shuffle<T>(this IList<T> list) {
		System.Random rng = new System.Random();
		int n = list.Count;
		while (n > 1) {
			n--;
			int k = rng.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}


	public static string GetTimeString(int time) {
		int h = (int)time / 60;
		int m = (int)time % 60;

		if (h >= 24)
			h -= 24;

		return string.Format("{0:00}" + ":" + "{1:00}", h, m);
	}
    
    public static string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }
}

