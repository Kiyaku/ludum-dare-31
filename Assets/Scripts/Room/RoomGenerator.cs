﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomGenerator : MonoBehaviour {
	public static RoomGenerator ins;
	public static int RoomSize = 16;
	public static int DecayTimer = 10;

	public Room RoomPrefab;
	[HideInInspector]
	public List<Room> ActiveRooms = new List<Room>();
	public List<Transform> SpawnableObjects = new List<Transform>();
	public Color DecayColor;
	public Material RoomMaterial;
	public int MinExtraRooms = 3;
	public int MaxExtraRooms = 9;
	public float RoomSpawnInterval = 5;

	public List<RoomData.Tile> TextureSetup = new List<RoomData.Tile>();

	// TEMP
	public List<RoomData> RoomDataList = new List<RoomData>();
	public Room startRoom;
	private Room lastRoom;

	private List<Vector2> dirList = new List<Vector2>();


	private void Awake() {
		ins = this;

		dirList.Add(new Vector2(1, 0));
		dirList.Add(new Vector2(-1, 0));
		dirList.Add(new Vector2(0, 1));
		dirList.Add(new Vector2(0, -1));

		foreach (Transform child in transform) {
			Room r = child.GetComponent<Room>();

			if(r != null) {
				ActiveRooms.Add(r);
			}
		}

		GameManager.OnStartGame += StartGenerating;
		GameManager.OnEndGame += EndGame;
	}


	private void OnDisable() {
		GameManager.OnStartGame -= StartGenerating;
		GameManager.OnEndGame -= EndGame;
	}

	
	private void EndGame() {
		StopAllCoroutines();
	}

	
	// Call to start the system
	public void StartGenerating() {
		StartCoroutine(AutoGenerateRooms());
		StartCoroutine(GenerateNeighbours(startRoom, Random.Range(MinExtraRooms, MaxExtraRooms)));
        Snowman.reset();
	}


	public Room GetRandomRoom() {
		Utils.Shuffle(ActiveRooms);

		if (ActiveRooms.Count > 0)
			return ActiveRooms[0];
		else
			return null;
	}


	private IEnumerator AutoGenerateRooms() {
		while (true) {
			// TODO: Change lastRoom with player later
			if (ActiveRooms.Count > 2 && lastRoom != null) {
				Utils.Shuffle(ActiveRooms);
				bool skip = false;

				// TODO: Only spawn far away from player sometimes
				//ActiveRooms.Sort((r1, r2) => Vector3.Distance(r2.transform.position, lastRoom.transform.position).CompareTo(Vector3.Distance(r1.transform.position, lastRoom.transform.position)));

				foreach (Room room in ActiveRooms) {
					for (int i = 0; i < 4; i++) {
						if (!skip) {
							int newPosX = room.PosX + (int)dirList[i].x;
							int newPosZ = room.PosZ + (int)dirList[i].y;

							Room neighbour = ActiveRooms.Find(r => r.PosX == newPosX && r.PosZ == newPosZ);

							if (neighbour == null) {
								// Speed up room spawning
								RoomSpawnInterval -= 0.1f;
								StartCoroutine(GenerateNeighbours(room, Random.Range(MinExtraRooms, MaxExtraRooms), true));
								skip = true;
							}
						}
					}

					if (skip)
						break;
				}
			}

			RoomSpawnInterval = Mathf.Clamp(RoomSpawnInterval, 1, RoomSpawnInterval);

			yield return new WaitForSeconds(RoomSpawnInterval);
		}
	}


	public IEnumerator GenerateNeighbours(Room centerRoom, int remainingRooms, bool persistent = false) {
		Utils.Shuffle(dirList);
		Room newRoom = null;

		for (int i = 0; i < 4; i++) {
			int newPosX = centerRoom.PosX + (int)dirList[i].x;
			int newPosZ = centerRoom.PosZ + (int)dirList[i].y;

			Room neighbour = ActiveRooms.Find(r => r.PosX == newPosX && r.PosZ == newPosZ);

			if (neighbour != null) {
				continue;
			}

			// PLACE ROOM HERE
			newRoom = PlaceRoom(newPosX, newPosZ);
			remainingRooms--;

			yield return new WaitForSeconds(0.15f);

			// Chance to additional rooms or continue with new room
			if (!persistent && Random.Range(0f, 1f) > 0.5f) {
				break;
			}
		}

		if (newRoom != null && remainingRooms > 0) {
			StartCoroutine(GenerateNeighbours(newRoom, remainingRooms, persistent));
		}

		yield return null;
	}


	public void RemoveRoom(Room room) {
		ActiveRooms.Remove(room);
	}


	public Room PlaceRoom(int posX, int posZ, RoomData data = null) {
		Room newRoom = Instantiate(RoomPrefab) as Room;
		newRoom.transform.parent = transform;

		if (data == null) {
			data = RoomDataList[Random.Range(0, RoomDataList.Count)];
		}

		newRoom.Data = data;
		newRoom.PosX = posX;
		newRoom.PosZ = posZ;

		newRoom.Init();

		lastRoom = newRoom;

		ActiveRooms.Add(newRoom);

		// Place spawnable
		if(Random.Range(0, 100) > 30) {
			Vector3 spawnPos = newRoom.GetSpawnableTiles();

			Transform spawnedObject = Instantiate(SpawnableObjects[Random.Range(0, SpawnableObjects.Count)]) as Transform;
			spawnedObject.position = spawnPos;
			spawnedObject.parent = newRoom.transform;
		}

		return newRoom;
	}
}
