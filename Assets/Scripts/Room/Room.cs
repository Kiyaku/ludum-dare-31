﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class Room : MonoBehaviour {
	public RoomData Data;
	public bool CanDecay = true;
	public bool InitOnStart = false;
	public bool AffectCamera = true;
	public int PosX = 0;
	public int PosZ = 0;
	public ParticleSystem Particles;


	private void Start() {
		if (InitOnStart)
			Init();

		audio.pitch = Random.Range(0.9f, 1.1f);

		if (AudioManager.PlaySFX)
			audio.Play();

		GameManager.OnEndGame += EndGame;
	}


	private void OnDisable() {
		GameManager.OnEndGame -= EndGame;
		GameManager.OnStartGame -= StartDecay;
	}


	private void EndGame() {
		StopAllCoroutines();
	}


	public void Init() {
		renderer.sharedMaterial = RoomGenerator.ins.RoomMaterial;
		transform.position = new Vector3(PosX * Data.RoomWidth, 0, PosZ * Data.RoomHeight);
		MeshCreator.CreateMesh(transform, Data);

		transform.Rotate(Vector3.up, Random.Range(0, 4) * 90);
		
		//StartCoroutine(AnimateRoom());

		if (CanDecay) {
			if (GameManager.ins.IsGameRunning) {
				StartCoroutine(StartDecaying());
			} else {
				GameManager.OnStartGame += StartDecay;
			}
		} 
	}


	// Returns an empty tile within the room
	public Vector3 GetSpawnableTiles() {
		Vector3 tilePos = default(Vector3);

		bool foundTile = false;
		Quaternion tempRot = transform.rotation;

		while (!foundTile) {
			int randomIndex = Random.Range(0, Data.Tiles.Length);
			RoomData.Tile textureTile = RoomGenerator.ins.TextureSetup.Find(t => t.TextureID == Data.Tiles[randomIndex].TextureID);

			if (!textureTile.IsWall) {
				foundTile = true;
				tilePos = new Vector3((randomIndex % Data.RoomWidth) + 0.5f, 0, (randomIndex / Data.RoomHeight) + 0.5f);
				tilePos -= new Vector3(Data.RoomWidth / 2, 0, Data.RoomHeight / 2);
			}
		}

		tilePos = transform.rotation * tilePos;

		return tilePos + transform.position;
	}


	private void StartDecay() {
		GameManager.OnStartGame -= StartDecay;
		StartCoroutine(StartDecaying());
	}


	private IEnumerator AnimateRoom() {
		float timer = 0;
		transform.localScale = Vector3.zero;

		while (timer < 1) {
			timer += Time.deltaTime * 2f;
			transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, timer);

			yield return null;
		}

		transform.localScale = Vector3.one;
	}


	private IEnumerator StartDecaying() {
		float timer = 0;
		Color startCol = Color.white;
		Color endCol = RoomGenerator.ins.DecayColor;
		float alpha = 1;

		do {
			yield return new WaitForSeconds(RoomGenerator.DecayTimer);
		} while (Random.Range(0, 10) > 2);

		while (timer < 5) {
			timer += Time.deltaTime;
			renderer.material.color = Color.Lerp(startCol, endCol, timer / 3f);

			yield return null;
		}

		RoomGenerator.ins.RemoveRoom(this);

		while (alpha > 0) {
			alpha -= Time.deltaTime;

			endCol.a = alpha;
			renderer.material.color = endCol;

			yield return null;
		}

		Destroy(gameObject);
	}
}
