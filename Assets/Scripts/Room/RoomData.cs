﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomData : ScriptableObject {
	[System.Serializable]
	public class Tile {
		public int TextureID = 0;
		public bool IsWall = false;
	}

	public Texture2D TileTexture;
	public int TexturesPerRow = 1;

	public int RoomWidth = 8;
	public int RoomHeight = 8;

	public Tile[] Tiles;


	public void Init() {
		RoomWidth = RoomGenerator.RoomSize;
		RoomHeight = RoomGenerator.RoomSize;

		Tiles = new Tile[RoomWidth * RoomHeight];
	}
}
