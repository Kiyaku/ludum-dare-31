﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MeshCreator {
	public static void CreateMesh(Transform room, RoomData data) {
		MeshRenderer meshRenderer = room.GetComponent<MeshRenderer>();

		// Make sure this target room has a mesh renderer
		if (meshRenderer != null) {
			MeshFilter meshFilter = room.GetComponent<MeshFilter>();
			Mesh mesh = new Mesh();

			List<Vector3> vertices = new List<Vector3>();
			List<int> triangles = new List<int>();
			List<Vector2> uvs = new List<Vector2>();

			int currentVert = 0;
			float uvOffset = 0.001f;
			Vector3 offset = new Vector3(data.RoomWidth / 2, 0, data.RoomHeight / 2);

			for (int x = 0; x < data.RoomWidth; x++) {
				for (int y = 0; y < data.RoomHeight; y++) {
					int textureID = data.Tiles[x + y * data.RoomWidth].TextureID;
					int yOffset = 0;

					// Check for colliders
					RoomData.Tile textureTile = RoomGenerator.ins.TextureSetup.Find(t => t.TextureID == textureID);

					if (textureTile != null && textureTile.IsWall) {
						yOffset = 1;
					}

					vertices.Add(new Vector3(x, yOffset, y) - offset);
					vertices.Add(new Vector3(x + 1, yOffset, y) - offset);
					vertices.Add(new Vector3(x + 1, yOffset, y + 1) - offset);
					vertices.Add(new Vector3(x, yOffset, y + 1) - offset);

					triangles.Add(currentVert + 2);
					triangles.Add(currentVert + 1);
					triangles.Add(currentVert);

					triangles.Add(currentVert + 3);
					triangles.Add(currentVert + 2);
					triangles.Add(currentVert);

					float uvX = textureID;
					float uvY = data.TexturesPerRow - 1;
					float tileSize = 1f / (float)data.TexturesPerRow;
					
					while (uvX >= data.TexturesPerRow) {
						uvX -= data.TexturesPerRow;
						uvY--;
					}

					uvX *= tileSize;
					uvY *= tileSize;

					Vector2 bottomLeft = new Vector2(Mathf.Abs(uvX) + uvOffset, uvY + uvOffset);
					Vector2 topLeft = new Vector2(Mathf.Abs(uvX) + uvOffset, uvY + tileSize - uvOffset);
					Vector2 topRight = new Vector2(Mathf.Abs(uvX + tileSize) - uvOffset, uvY + tileSize - uvOffset);
					Vector2 bottomRight = new Vector2(Mathf.Abs(uvX + tileSize) - uvOffset, uvY + uvOffset);

					uvs.Add(bottomLeft);
					uvs.Add(bottomRight);
					uvs.Add(topRight);
					uvs.Add(topLeft);

					currentVert += 4;
				}
			}

			mesh.vertices = vertices.ToArray();
			mesh.triangles = triangles.ToArray();
			mesh.uv = uvs.ToArray();

			mesh.RecalculateNormals();
			room.gameObject.AddComponent<MeshCollider>().sharedMesh = mesh;

			meshFilter.mesh = mesh;
			room.renderer.material.SetTexture("_MainTex", data.TileTexture);
		}
	}
}
