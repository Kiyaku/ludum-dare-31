﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IInteractive {
	void Interact(Transform target);
}
