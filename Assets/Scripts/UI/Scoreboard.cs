﻿using UnityEngine;
using System.Collections;

public class Scoreboard : MonoBehaviour {

    public UILabel[] rows;


    public void displayScores(JSONObject scores)
    {
        for (int ii = 0; ii < scores.list.Count; ii++)
        {
            rows[ii].text = (ii+1) + ".";
            UILabel[] parts = rows[ii].GetComponentsInChildren<UILabel>();
            parts[1].text = scores[ii].GetField("name").ToString().Replace("\"", "");
            parts[2].text = scores[ii].GetField("time").ToString().Replace("\"", "");
            parts[3].text = scores[ii].GetField("score").ToString();
        }
    }
}
