﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIManager : MonoBehaviour {
	[System.Serializable]
	public class StatLabel {
		public UILabel Label;
		public TweenScale Tween;
		public int Type;
	}
    
	public static UIManager ins;
	public UISprite SpeechBubble;
	public UILabel SpeechBubbleLabel; 

	public List<StatLabel> StatLabels = new List<StatLabel>();

	public UILabel PickupLabel;
	public UISprite PickupSprite;
	public UILabel TimeLabel;
	public UIPanel GameOverPanel;
	public Player player;

	//Highscore End Screen
	public UILabel ScoreLabelHealth;
	public UILabel ScoreLabelShield;
	public UILabel ScoreLabelAttack;
	public UILabel ScoreLabelSpeed;
	public UILabel ScoreLabelTime;
	public UILabel ScoreLabelKills;
    public UILabel ScoreLabelTotal;
    public UILabel RestartLabel;

    public GameObject ScoreBoard;
    public UIInput ScoreBoardNameEntry;
    public UIButton scoreBoardBtn;
    public UILabel PlayerNameLabel;
    // Save the name they type in the first time...
    public static string PlayerName = "A Player";

    

	public DamageNumber DamageNumberPrefab;
	public UIPanel GamePanel;

	private float speechBubbleTimer = 0;
	private Transform tempTarget;
	private int time;
	private List<DamageNumber> damageNumbers = new List<DamageNumber>();

	private int pointsPerHealth = 100;
	private int pointsPerAttack= 100;
	private int pointsPerShield = 100;
	private int pointsPerSpeed = 100;
	private int pointsPerKills = 1000;
	private int pointsPerTime = 10;

	private string[] pickupSpriteNames = new string[] {"Heart", "Sword", "Shield", "Speed", "Scroll"};

    private int totalScore = 0;

	private void Awake() {
		ins = this;
        
        GameManager.OnStartGame += StartTimer;
		GameManager.OnEndGame += EndGame;

		StartCoroutine(AnimateSpeechBubble());
	}


	private void OnDisable() {
		GameManager.OnStartGame -= StartTimer;
		GameManager.OnEndGame -= EndGame;
	}


	private void EndGame() {
		int scoreHealth = (player.getStat(0) * pointsPerHealth);
		int scoreShield = (player.getStat(2) * pointsPerShield);
		int scoreAttack = (player.getStat(1) * pointsPerAttack);
		int scoreSpeed = (player.getStat(3) * pointsPerSpeed);
		int scoreKills = (player.GetComponent<PlayerMove>().Kills * pointsPerKills);
		int scoreTime = time * pointsPerTime;
        int scoreTotal = totalScore = (scoreHealth + scoreShield + scoreAttack + scoreSpeed + scoreTime + scoreKills);

		ScoreLabelHealth.text = "x[d91640]" + player.getStat(0) + "[-] = " + scoreHealth;
		ScoreLabelShield.text = "x[d91640]" + player.getStat(2) + "[-] = " + scoreShield;
		ScoreLabelAttack.text = "x[d91640]" + player.getStat(1) + "[-] = " + scoreAttack;
		ScoreLabelSpeed.text = "x[d91640]" + player.getStat(3) + "[-] = " + scoreSpeed;
		ScoreLabelTime.text = "Time ([d91640]" + Utils.GetTimeString(time) + "[-]) = " + scoreTime;
		ScoreLabelKills.text = "Kills x[d91640]" + player.GetComponent<PlayerMove>().Kills + "[-] = " + scoreKills;
        ScoreLabelTotal.text = "Total Score: [d91640]" + scoreTotal + "[-]";

        PlayerNameLabel.text = PlayerName;

		ScoreLabelHealth.gameObject.SetActive(false);
		ScoreLabelShield.gameObject.SetActive(false);
		ScoreLabelAttack.gameObject.SetActive(false);
		ScoreLabelSpeed.gameObject.SetActive(false);
		ScoreLabelTime.gameObject.SetActive(false);
		ScoreLabelKills.gameObject.SetActive(false);
		ScoreLabelTotal.gameObject.SetActive(false);
        RestartLabel.gameObject.SetActive(false);
        ScoreBoardNameEntry.gameObject.SetActive(false);

		GamePanel.gameObject.SetActive(false);
		SpeechBubble.gameObject.SetActive(false);

        StopAllCoroutines();
        StartCoroutine(AnimateEndScreen());
	}


	private IEnumerator AnimateEndScreen() {
		float delay = 0.3f;

		yield return new WaitForSeconds(1);
		GameOverPanel.gameObject.SetActive(true);
		ScoreLabelHealth.gameObject.SetActive(true);
		yield return new WaitForSeconds(delay);
		ScoreLabelShield.gameObject.SetActive(true);
		yield return new WaitForSeconds(delay);
		ScoreLabelAttack.gameObject.SetActive(true);
		yield return new WaitForSeconds(delay);
		ScoreLabelSpeed.gameObject.SetActive(true);
		yield return new WaitForSeconds(delay);
		ScoreLabelTime.gameObject.SetActive(true);
		yield return new WaitForSeconds(delay);
		ScoreLabelKills.gameObject.SetActive(true);
		yield return new WaitForSeconds(delay);
		ScoreLabelTotal.gameObject.SetActive(true);
        yield return new WaitForSeconds(delay);
        RestartLabel.gameObject.SetActive(true);
        yield return new WaitForSeconds(delay);
        ScoreBoardNameEntry.gameObject.SetActive(true);
        yield return new WaitForSeconds(delay);
        scoreBoardBtn.enabled = true;
	}


    public void SubmitScore() {
        scoreBoardBtn.isEnabled = false;
        PlayerName = PlayerNameLabel.text;
        StartCoroutine(submitScore(totalScore));
		ScoreBoardNameEntry.GetComponent<UITweener>().PlayReverse();
    }

    private IEnumerator submitScore(int scoreTotal)
    {
        string url = "http://xekeland.com/games/ld31/ajax/submit";
        WWWForm form = new WWWForm();
        form.AddField("name", PlayerName);
        form.AddField("health", player.getStat(0));
        form.AddField("attack", player.getStat(1));
        form.AddField("shield", player.getStat(2));
        form.AddField("speed", player.getStat(3));
        form.AddField("kills", player.GetComponent<PlayerMove>().Kills);

        form.AddField("time", Utils.GetTimeString(time));
        form.AddField("score", scoreTotal);

        string key = "h2oTT3ceVCx2xqRD";

        string hash = PlayerName + player.getStat(3) + player.getStat(2) + player.getStat(1) + player.getStat(0) + key;
        form.AddField("code", Utils.Md5Sum(hash));

        WWW req = new WWW(url, form);
        yield return req;

        JSONObject parser;

        if (req.error != null)
        {
            parser = new JSONObject(req.text);
        }
        else
        {
            string testData = "[{\"id\":18,\"name\":\"Thungon\",\"health\":23,\"shield\":17,\"attack\":9,\"speed\":17,\"kills\":10,\"time\":\"08:31\",\"score\":21710},{\"id\":19,\"name\":\"Thungon\",\"health\":3,\"shield\":17,\"attack\":9,\"speed\":12,\"kills\":7,\"time\":\"07:04\",\"score\":15340},{\"id\":20,\"name\":\"Thungon\",\"health\":0,\"shield\":10,\"attack\":8,\"speed\":10,\"kills\":2,\"time\":\"06:27\",\"score\":8670},{\"id\":14,\"name\":\"KrystalWhisper\",\"health\":0,\"shield\":6,\"attack\":10,\"speed\":5,\"kills\":7,\"time\":\"05:40\",\"score\":12500},{\"id\":23,\"name\":\"Thungon\",\"health\":79,\"shield\":8,\"attack\":16,\"speed\":6,\"kills\":6,\"time\":\"05:33\",\"score\":20230},{\"id\":22,\"name\":\"Thungon\",\"health\":36,\"shield\":5,\"attack\":10,\"speed\":10,\"kills\":7,\"time\":\"04:59\",\"score\":16090},{\"id\":17,\"name\":\"KrystalWhisper\",\"health\":33,\"shield\":8,\"attack\":12,\"speed\":6,\"kills\":6,\"time\":\"04:38\",\"score\":14680},{\"id\":8,\"name\":\"KrystalWhisper\",\"health\":27,\"shield\":8,\"attack\":6,\"speed\":4,\"kills\":2,\"time\":\"04:34\",\"score\":9240},{\"id\":12,\"name\":\"KrystalWhisper\",\"health\":18,\"shield\":6,\"attack\":6,\"speed\":6,\"kills\":2,\"time\":\"03:12\",\"score\":7520},{\"id\":15,\"name\":\"KrystalWhisper\",\"health\":17,\"shield\":4,\"attack\":3,\"speed\":2,\"kills\":1,\"time\":\"02:49\",\"score\":5290}]";
            parser = new JSONObject(testData);
        }
        ScoreBoard.GetComponent<Scoreboard>().displayScores(parser);
        

    }

	private void StartTimer() {
		time = 0;

		StartCoroutine(Timer());
	}


	private IEnumerator Timer() {
		do {
			time++;

			TimeLabel.text = "Time: [d91640]" + Utils.GetTimeString(time) + "[-]";

			yield return new WaitForSeconds(1);
		} while (true);
	}


	// Updates stats and animates them
	public void UpdateStat(int type, int value, bool useX, bool showLabel = true) {
		StatLabel label = StatLabels.Find(l => l.Type == type);

		if (showLabel) {
			PickupLabel.text = "+1";
			PickupSprite.spriteName = pickupSpriteNames[type];

			StopCoroutine(AnimatePickupSprite());
			StartCoroutine(AnimatePickupSprite());
		}

		if (label != null) {
			label.Label.text = "";

			if (useX) {
				label.Label.text = "x";
			}

			label.Label.text += value;
			label.Tween.ResetToBeginning();
			label.Tween.PlayForward();
		}
	}


	private IEnumerator AnimatePickupSprite() {
		float timer = 0;
		
		PickupLabel.gameObject.SetActive(true);

		while (timer < 2) {
			timer += Time.deltaTime * 2f;

			Vector3 startPos = GetScreenPos(player.transform.position) + new Vector3(0, 10, 0);
			Vector3 endPos = startPos + new Vector3(0, 20, 0);

			PickupLabel.transform.localPosition = Vector3.Lerp(startPos, endPos, timer);
		

			yield return null;
		}

		PickupLabel.gameObject.SetActive(false);
	}


	public void ShowSpeechBubble(Transform target, string text) {
		speechBubbleTimer = 0;
		tempTarget = target;
		
		SpeechBubbleLabel.text = text;
		SpeechBubble.gameObject.SetActive(true);
		SpeechBubble.GetComponent<TweenScale>().ResetToBeginning();
		SpeechBubble.GetComponent<TweenScale>().PlayForward();
	}


	private Vector3 GetScreenPos(Vector3 worldPos) {
		Vector3 screenPos = Camera.main.WorldToViewportPoint(worldPos);
		Vector3 screenSize = new Vector3(Screen.width * UIRoot.GetPixelSizeAdjustment(SpeechBubble.gameObject), Screen.height * UIRoot.GetPixelSizeAdjustment(SpeechBubble.gameObject), 0);

		screenPos.x *= screenSize.x;
		screenPos.y *= screenSize.y;
		screenPos.x -= screenSize.x / 2f;
		screenPos.y -= screenSize.y / 2f;

		return screenPos;
	}


	private IEnumerator AnimateSpeechBubble() {
		float alpha = 1;
		float maxDisplayTime = 2;

		while (true) {
			speechBubbleTimer += Time.deltaTime;

			if (speechBubbleTimer <= maxDisplayTime + 1 && tempTarget != null) {
				if (speechBubbleTimer >= maxDisplayTime) {
					alpha -= Time.deltaTime * 2f;
				} else {
					alpha = 1;
				}

				SpeechBubble.alpha = alpha;
				
				//SpeechBubble.transform.localPosition = NGUIMath.WorldToLocalPoint(tempTarget.position + new Vector3(0, 1.7f, 0), Camera.main, UICamera.currentCamera, tempTarget);
				SpeechBubble.transform.localPosition = GetScreenPos(tempTarget.position + new Vector3(0, 1.7f, 0));
			}

			yield return null;
		}
	}


	// Damage numbers
	public void ShowDamageNumber(int damage, Vector3 pos) {
		DamageNumber selectedNumber = null;

		foreach (DamageNumber number in damageNumbers) {
			if (number.IsAvailable) {
				selectedNumber = number;
				break;
			}
		}

		if (selectedNumber == null) {
			selectedNumber = NGUITools.AddChild(GamePanel.gameObject, DamageNumberPrefab.gameObject).GetComponent<DamageNumber>();
			selectedNumber.transform.parent = GamePanel.transform;

			damageNumbers.Add(selectedNumber);
		}

		selectedNumber.Animate(GetScreenPos(pos + new Vector3(0, 1.7f, 0)), damage);
	}
}
