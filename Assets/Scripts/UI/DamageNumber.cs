﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DamageNumber : MonoBehaviour {
	public bool IsAvailable = true;
	private UILabel label;


	public void Animate(Vector3 pos, int dmg) {
		label = GetComponent<UILabel>();
		label.text = dmg.ToString();
		StartCoroutine(AnimateText(pos));
	}


	private IEnumerator AnimateText(Vector3 pos) {
		float timer = 0;
		Vector3 endPos = pos + new Vector3(0, 50, 0);

		transform.localPosition = pos;

		label.enabled = true;
		IsAvailable = false;

		while (timer < 1) {
			timer += Time.deltaTime * 3f;

			transform.localPosition = Vector3.Lerp(transform.localPosition, endPos, timer);

			yield return null;
		}

		yield return new WaitForSeconds(0.5f);

		label.enabled = false;
		IsAvailable = true;
	}
}
