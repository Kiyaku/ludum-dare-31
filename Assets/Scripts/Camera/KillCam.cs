﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KillCam : MonoBehaviour {
	public static KillCam ins;
	public Transform Player;
	private float tanAngle;
	private float orthoSize = 2;
	private Camera camera;


	private void Awake() {
		ins = this;

		camera = GetComponent<Camera>();
		camera.orthographicSize = orthoSize;
		tanAngle = Mathf.Tan(30 * Mathf.Deg2Rad);

		ToggleCamera(false);
	}


	public void ToggleCamera(bool enabled) {
		camera.enabled = enabled;
	}


	public void Update() {
		float posX = Player.position.x;
		float posY = Player.position.z;

		Vector3 camPos = new Vector3(posX, transform.position.y, posY + 1);
		camPos.y = orthoSize / 2 + 2;
		camPos.z -= camPos.y * tanAngle;

		transform.position = camPos;
	}
}
