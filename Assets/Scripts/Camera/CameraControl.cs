﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraControl : MonoBehaviour {
	public static CameraControl ins;

	public enum CameraModes {
		EXPLORING,
		FIGHTING
	}

	public Transform LevelContainer;
	public Transform Player;
	public AudioClip SplashSound;
	public AudioSource MusicPlayer;

	private Vector3 newCamPos;
	private float newOrthoSize;
	private float tanAngle;
	private float shakeTimer = 0;
	private CameraModes cameraMode = CameraModes.EXPLORING;


	private void Start() {
		ins = this;

		AudioManager.OnMusicChange += ToggleMusic;

		ToggleMusic();
		
		tanAngle = Mathf.Tan(30 * Mathf.Deg2Rad);

		newCamPos = transform.position;
		newOrthoSize = Camera.main.orthographicSize;
	}


	private void OnDisable() {
		AudioManager.OnMusicChange -= ToggleMusic;
	}


	private void ToggleMusic() {
		if (AudioManager.PlayMusic)
			MusicPlayer.Play();
		else
			MusicPlayer.Stop();
	}


	private void Update() {
		if (!GameManager.ins.IsGameOver) {
			UpdateCamera();
		} else if (shakeTimer < 1) {
			shakeTimer = 1;
			MusicPlayer.Stop();
			StartCoroutine(ShakeCam());
		}

		transform.position = Vector3.Lerp(transform.position, newCamPos, Time.deltaTime * 3f);
		Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, newOrthoSize, Time.deltaTime * 5f);
	}


	private IEnumerator ShakeCam() {
		float timer = 0;

		yield return new WaitForSeconds(0.6f);

		if(AudioManager.PlaySFX)
			audio.PlayOneShot(SplashSound);

		while (timer < 1) {
			timer += Time.deltaTime * 8f;

			float shakeDist = newOrthoSize / 20f;

			transform.position = newCamPos + new Vector3(Random.Range(-shakeDist, shakeDist), 0, Random.Range(-shakeDist, shakeDist));

			yield return null;
		}
	}


	public void SwitchCameraMode(CameraModes mode) {
		cameraMode = mode;
	}


	// Zoom out camera depending on world bound
	public void UpdateCamera() {
		float minX = 0;
		float minY = 0;
		float maxX = 0;
		float maxY = 0;

		if (cameraMode == CameraModes.EXPLORING) {
			foreach (Transform room in LevelContainer) {
				if (!room.GetComponent<Room>().AffectCamera)
					continue;

				Bounds bound = room.renderer.bounds;

				minX = (bound.min.x < minX) ? bound.min.x : minX;
				maxX = (bound.max.x > maxX) ? bound.max.x : maxX;

				minY = (bound.min.z < minY) ? bound.min.z : minY;
				maxY = (bound.max.z > maxY) ? bound.max.z : maxY;
			}

			float cameraX = (minX + maxX) / 2;
			float cameraZ = (minY + maxY) / 2;

			float absMaxX = Mathf.Abs(maxX - cameraX);
			float absMaxY = Mathf.Abs(maxY - cameraZ);
			float absMinX = Mathf.Abs(minX - cameraX);
			float absMinY = Mathf.Abs(minY - cameraZ);

			float widthModifier = (float)Screen.width / (float)Screen.height;

			float height = (absMaxY > absMinY) ? absMaxY : absMinY;
			float width = ((absMaxX > absMinX) ? absMaxX : absMinX) / widthModifier;

			newOrthoSize = (height > width) ? height : width;
		} else {
			minX = Player.position.x;
			maxX = minX;

			minY = Player.position.z;
			maxY = minY;

			newOrthoSize = 8;
		}

		newOrthoSize = Mathf.Clamp(newOrthoSize, 8, newOrthoSize);
		
		newCamPos = new Vector3((minX + maxX) / 2, transform.position.y, (minY + maxY) / 2);
		newCamPos.y = (newOrthoSize / 2) + 2;
		newCamPos.z -= newCamPos.y * tanAngle;
	}
}
